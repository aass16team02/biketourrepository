package com.example.biketour;

import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class GatowActivity extends FragmentActivity {

    public GoogleMap map;
    public final LatLng START_LOCATION = new LatLng(52.517716966,13.399288943);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gatow);

        map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.addMarker(new MarkerOptions().position(START_LOCATION));
        map.setMyLocationEnabled(true);

        map.addPolyline(new PolylineOptions().geodesic(true)
                .add(new LatLng(52.517716966,13.399288943))
                .add(new LatLng(52.517643884,13.398670605))
                .add(new LatLng(52.517598526,13.39844987))
                .add(new LatLng(52.517570685,13.397993344))
                .add(new LatLng(52.517543128,13.39768413))
                .add(new LatLng(52.517477507,13.396285001))
                .add(new LatLng(52.517459133,13.39607886))
                .add(new LatLng(52.517413195,13.395563507))
                .add(new LatLng(52.517403057,13.394974311))
                .add(new LatLng(52.517383901,13.394370431))
                .add(new LatLng(52.51729954,13.392603131))
                .add(new LatLng(52.517207808,13.391660823))
                .add(new LatLng(52.517151955,13.390688864))
                .add(new LatLng(52.517022459,13.388848169))
                .add(new LatLng(52.516724936,13.383944346))
                .add(new LatLng(52.516586319,13.382089007))
                .add(new LatLng(52.516547688,13.380792897))
                .add(new LatLng(52.516491192,13.379555813))
                .add(new LatLng(52.516463526,13.379217156))
                .add(new LatLng(52.516335184,13.378009944))
                .add(new LatLng(52.516235031,13.377391813))
                .add(new LatLng(52.516260904,13.376876086))
                .add(new LatLng(52.516222622,13.375771489))
                .add(new LatLng(52.515989844,13.371972273))
                .add(new LatLng(52.515961457,13.371309553))
                .add(new LatLng(52.51588657,13.369925296))
                .add(new LatLng(52.515868054,13.369674982))
                .add(new LatLng(52.515644683,13.366155654))
                .add(new LatLng(52.515309845,13.36106086))
                .add(new LatLng(52.515216799,13.35964729))
                .add(new LatLng(52.515123937,13.358322108))
                .add(new LatLng(52.514705257,13.352093714))
                .add(new LatLng(52.514614298,13.351622909))
                .add(new LatLng(52.514883174,13.351311892))
                .add(new LatLng(52.515044485,13.351119388))
                .add(new LatLng(52.515151843,13.350912489))
                .add(new LatLng(52.515249973,13.350602534))
                .add(new LatLng(52.515294321,13.350351836))
                .add(new LatLng(52.515311641,13.350071847))
                .add(new LatLng(52.515283958,13.349762679))
                .add(new LatLng(52.515211239,13.349409603))
                .add(new LatLng(52.515084773,13.349130519))
                .add(new LatLng(52.514976693,13.349028087))
                .add(new LatLng(52.514859627,13.348925711))
                .add(new LatLng(52.514742595,13.348838067))
                .add(new LatLng(52.514391949,13.348766626))
                .add(new LatLng(52.514381232,13.348030181))
                .add(new LatLng(52.514035744,13.34256748))
                .add(new LatLng(52.513738207,13.338444984))
                .add(new LatLng(52.51369227,13.338032844))
                .add(new LatLng(52.513579024,13.335824085))
                .add(new LatLng(52.513400569,13.332702515))
                .add(new LatLng(52.513343975,13.331627607))
                .add(new LatLng(52.513351969,13.331229846))
                .add(new LatLng(52.513295768,13.330316968))
                .add(new LatLng(52.51329551,13.33021386))
                .add(new LatLng(52.513267812,13.329919449))
                .add(new LatLng(52.513015179,13.325958828))
                .add(new LatLng(52.512903478,13.324442422))
                .add(new LatLng(52.512884376,13.32400066))
                .add(new LatLng(52.512874862,13.323794509))
                .add(new LatLng(52.512865084,13.32348525))
                .add(new LatLng(52.512846545,13.323264432))
                .add(new LatLng(52.51290877,13.322998867))
                .add(new LatLng(52.513051987,13.322776932))
                .add(new LatLng(52.513132298,13.32255543))
                .add(new LatLng(52.513185687,13.322348844))
                .add(new LatLng(52.513203432,13.322260342))
                .add(new LatLng(52.513238848,13.322053878))
                .add(new LatLng(52.51324719,13.321803412))
                .add(new LatLng(52.513219661,13.321582655))
                .add(new LatLng(52.513183183,13.321376689))
                .add(new LatLng(52.513137909,13.321244434))
                .add(new LatLng(52.513092673,13.321126908))
                .add(new LatLng(52.513011262,13.320921255))
                .add(new LatLng(52.512876123,13.320789625))
                .add(new LatLng(52.512588137,13.320629597))
                .add(new LatLng(52.512446917,13.318155999))
                .add(new LatLng(52.512363271,13.317096053))
                .add(new LatLng(52.512297123,13.315859231))
                .add(new LatLng(52.512249596,13.314872685))
                .add(new LatLng(52.512044216,13.311972425))
                .add(new LatLng(52.511848146,13.309219419))
                .add(new LatLng(52.511819012,13.30840951))
                .add(new LatLng(52.51179011,13.307687977))
                .add(new LatLng(52.51164175,13.305995172))
                .add(new LatLng(52.511585714,13.305214923))
                .add(new LatLng(52.511557234,13.304655415))
                .add(new LatLng(52.511379654,13.302152733))
                .add(new LatLng(52.511333185,13.301593362))
                .add(new LatLng(52.511256834,13.299973707))
                .add(new LatLng(52.511201084,13.299311305))
                .add(new LatLng(52.511098887,13.298104269))
                .add(new LatLng(52.511070823,13.29770679))
                .add(new LatLng(52.511042922,13.297368227))
                .add(new LatLng(52.511024293,13.297132699))
                .add(new LatLng(52.511005664,13.296897172))
                .add(new LatLng(52.510986829,13.296588))
                .add(new LatLng(52.510967911,13.296249371))
                .add(new LatLng(52.510910987,13.29517457))
                .add(new LatLng(52.51082701,13.294070518))
                .add(new LatLng(52.510732166,13.292303743))
                .add(new LatLng(52.510685277,13.291611832))
                .add(new LatLng(52.51066622,13.291229021))
                .add(new LatLng(52.5105348,13.289256333))
                .add(new LatLng(52.510515399,13.288755694))
                .add(new LatLng(52.510477138,13.287945892))
                .add(new LatLng(52.510421067,13.287195145))
                .add(new LatLng(52.51034613,13.286135236))
                .add(new LatLng(52.51028017,13.285075262))
                .add(new LatLng(52.510242182,13.284368568))
                .add(new LatLng(52.510186046,13.2836031))
                .add(new LatLng(52.510139276,13.282970122))
                .add(new LatLng(52.510101362,13.282292889))
                .add(new LatLng(52.510091003,13.281821645))
                .add(new LatLng(52.510072215,13.281541944))
                .add(new LatLng(52.510070883,13.281085359))
                .add(new LatLng(52.510005524,13.280246326))
                .add(new LatLng(52.509930481,13.279171712))
                .add(new LatLng(52.509827298,13.277699645))
                .add(new LatLng(52.509761218,13.276624967))
                .add(new LatLng(52.509639724,13.275049957))
                .add(new LatLng(52.509574234,13.274181485))
                .add(new LatLng(52.50968172,13.274062796))
                .add(new LatLng(52.509744274,13.273944465))
                .add(new LatLng(52.509841939,13.273546005))
                .add(new LatLng(52.509930309,13.273044517))
                .add(new LatLng(52.509974405,13.272764314))
                .add(new LatLng(52.510036164,13.272380868))
                .add(new LatLng(52.510071051,13.272027094))
                .add(new LatLng(52.509845243,13.271645962))
                .add(new LatLng(52.50980009,13.271572682))
                .add(new LatLng(52.509512215,13.2714719))
                .add(new LatLng(52.509421467,13.271178059))
                .add(new LatLng(52.509374764,13.270589289))
                .add(new LatLng(52.509337044,13.270000448))
                .add(new LatLng(52.50929038,13.269426409))
                .add(new LatLng(52.509251719,13.268528277))
                .add(new LatLng(52.509186361,13.267718735))
                .add(new LatLng(52.509063985,13.265893391))
                .add(new LatLng(52.508828907,13.262493036))
                .add(new LatLng(52.508761189,13.260932379))
                .add(new LatLng(52.5086487,13.259431013))
                .add(new LatLng(52.508582959,13.2585184))
                .add(new LatLng(52.508570624,13.25744333))
                .add(new LatLng(52.508551686,13.257134192))
                .add(new LatLng(52.508362957,13.254263749))
                .add(new LatLng(52.508182959,13.251334343))
                .add(new LatLng(52.508087852,13.24968559))
                .add(new LatLng(52.508039444,13.248596115))
                .add(new LatLng(52.507935692,13.247065272))
                .add(new LatLng(52.507754277,13.243753003))
                .add(new LatLng(52.507623072,13.242089868))
                .add(new LatLng(52.507309564,13.236908348))
                .add(new LatLng(52.507250447,13.235332976))
                .add(new LatLng(52.507098654,13.232918943))
                .add(new LatLng(52.507014048,13.231800378))
                .add(new LatLng(52.50690766,13.229547966))
                .add(new LatLng(52.507041863,13.229370022))
                .add(new LatLng(52.507095237,13.229207533))
                .add(new LatLng(52.50713913,13.22889785))
                .add(new LatLng(52.507083429,13.228368147))
                .add(new LatLng(52.507055825,13.228176934))
                .add(new LatLng(52.507046293,13.228015013))
                .add(new LatLng(52.507062926,13.227617208))
                .add(new LatLng(52.507558146,13.2252415))
                .add(new LatLng(52.508264865,13.221656077))
                .add(new LatLng(52.508901514,13.218630873))
                .add(new LatLng(52.508891512,13.218336394))
                .add(new LatLng(52.5089896,13.218114556))
                .add(new LatLng(52.509307404,13.216461996))
                .add(new LatLng(52.509349833,13.215739895))
                .add(new LatLng(52.510321721,13.210973353))
                .add(new LatLng(52.510763416,13.208803996))
                .add(new LatLng(52.511019125,13.207417019))
                .add(new LatLng(52.511716421,13.203860572))
                .add(new LatLng(52.512184309,13.201514065))
                .add(new LatLng(52.512281518,13.201056506))
                .add(new LatLng(52.513004886,13.197308111))
                .add(new LatLng(52.513074799,13.196762421))
                .add(new LatLng(52.513199109,13.196348767))
                .add(new LatLng(52.513666633,13.193943183))
                .add(new LatLng(52.514213413,13.191109583))
                .add(new LatLng(52.514105306,13.191037))
                .add(new LatLng(52.513385044,13.190675869))
                .add(new LatLng(52.512106538,13.187595291))
                .add(new LatLng(52.511680352,13.186568472))
                .add(new LatLng(52.510709644,13.184118391))
                .add(new LatLng(52.510502958,13.184120466))
                .add(new LatLng(52.510287011,13.18404899))
                .add(new LatLng(52.509783171,13.183892033))
                .add(new LatLng(52.508766448,13.183563492))
                .add(new LatLng(52.50881838,13.183032741))
                .add(new LatLng(52.508870807,13.182634542))
                .add(new LatLng(52.508976099,13.181955964))
                .add(new LatLng(52.509055371,13.181528033))
                .add(new LatLng(52.509178796,13.180893453))
                .add(new LatLng(52.509213798,13.180642711))
                .add(new LatLng(52.509222285,13.180510067))
                .add(new LatLng(52.509239369,13.180274234))
                .add(new LatLng(52.509220341,13.179994581))
                .add(new LatLng(52.509325896,13.179389633))
                .add(new LatLng(52.509590637,13.178105537))
                .add(new LatLng(52.509970562,13.17639311))
                .add(new LatLng(52.510358098,13.174327077))
                .add(new LatLng(52.510588193,13.173396777))
                .add(new LatLng(52.510676871,13.173086552))
                .add(new LatLng(52.511057629,13.171609699))
                .add(new LatLng(52.510939957,13.171389975))
                .add(new LatLng(52.510759551,13.171215088))
                .add(new LatLng(52.510542916,13.170966931))
                .add(new LatLng(52.510398568,13.170821133))
                .add(new LatLng(52.510190975,13.170587616))
                .add(new LatLng(52.509974452,13.170368922))
                .add(new LatLng(52.509802861,13.170149765))
                .add(new LatLng(52.509577009,13.169842799))
                .add(new LatLng(52.509232287,13.169006836))
                .add(new LatLng(52.509141455,13.168757391))
                .add(new LatLng(52.508996363,13.16842014))
                .add(new LatLng(52.508815439,13.168112721))
                .add(new LatLng(52.508598341,13.167746768))
                .add(new LatLng(52.508372483,13.167439823))
                .add(new LatLng(52.508137353,13.167059335))
                .add(new LatLng(52.507983957,13.166898924))
                .add(new LatLng(52.50791178,13.166826035))
                .add(new LatLng(52.507840004,13.166856241))
                .add(new LatLng(52.507533611,13.166638515))
                .add(new LatLng(52.507307923,13.166375766))
                .add(new LatLng(52.507109308,13.166142192))
                .add(new LatLng(52.506874345,13.165805905))
                .add(new LatLng(52.506639324,13.165454893))
                .add(new LatLng(52.506223772,13.164899589))
                .add(new LatLng(52.50589808,13.164343352))
                .add(new LatLng(52.503882592,13.161419105))
                .add(new LatLng(52.502951495,13.160029903))
                .add(new LatLng(52.502716283,13.159634776))
                .add(new LatLng(52.502255015,13.158888617))
                .add(new LatLng(52.502074357,13.158654912))
                .add(new LatLng(52.500835297,13.156665341))
                .add(new LatLng(52.500790249,13.15663637))
                .add(new LatLng(52.500129916,13.155553709))
                .add(new LatLng(52.49945207,13.154589072))
                .add(new LatLng(52.499271287,13.154325951))
                .add(new LatLng(52.498187162,13.152894511))
                .add(new LatLng(52.4981148,13.152777489))
                .add(new LatLng(52.497793061,13.153222695))
                .add(new LatLng(52.49771295,13.153414979))
                .add(new LatLng(52.49711673,13.152640971))
                .add(new LatLng(52.496547465,13.151866693))
                .add(new LatLng(52.495869767,13.150946362))
                .add(new LatLng(52.49559852,13.150537011))
                .add(new LatLng(52.494921168,13.14970506))
                .add(new LatLng(52.494379376,13.149063074))
                .add(new LatLng(52.494162457,13.148756226))
                .add(new LatLng(52.493349821,13.147808011))
                .add(new LatLng(52.493132481,13.147398116))
                .add(new LatLng(52.49214748,13.146054303))
                .add(new LatLng(52.490963986,13.144521324))
                .add(new LatLng(52.490503104,13.143893296))
                .add(new LatLng(52.489738667,13.143754443))
                .add(new LatLng(52.489405994,13.143713919))
                .add(new LatLng(52.489297978,13.143670936))
                .add(new LatLng(52.489278021,13.143185325))
                .add(new LatLng(52.489318798,13.142169053))
                .add(new LatLng(52.489342681,13.141417963))
                .add(new LatLng(52.489161687,13.141110791))
                .add(new LatLng(52.488601635,13.140410308))
                .add(new LatLng(52.488068476,13.139694824))
                .add(new LatLng(52.487824998,13.13949141))
                .add(new LatLng(52.487590627,13.13931734))
                .add(new LatLng(52.487356073,13.139099111))
                .add(new LatLng(52.48706657,13.138631228))
                .add(new LatLng(52.486821935,13.138148133))
                .add(new LatLng(52.486650041,13.137870334))
                .add(new LatLng(52.486460355,13.137636898))
                .add(new LatLng(52.486153665,13.137360598))
                .add(new LatLng(52.485927972,13.137112846))
                .add(new LatLng(52.485738285,13.136879416))
                .add(new LatLng(52.485520846,13.136454927))
                .add(new LatLng(52.485338679,13.13586812))
                .add(new LatLng(52.484489444,13.134788253))
                .add(new LatLng(52.481453522,13.130862564))
                .add(new LatLng(52.480938464,13.130191264))
                .add(new LatLng(52.480278755,13.129315543))
                .add(new LatLng(52.479763812,13.128673715))
                .add(new LatLng(52.479528434,13.128264245))
                .add(new LatLng(52.479202818,13.127767486))
                .add(new LatLng(52.478695361,13.126772361))
                .add(new LatLng(52.478513146,13.126185685))
                .add(new LatLng(52.478287306,13.125908597))
                .add(new LatLng(52.477681296,13.124988226))
                .add(new LatLng(52.477102738,13.124185312))
                .add(new LatLng(52.477066418,13.124097418))
                .add(new LatLng(52.476207337,13.122841477))
                .add(new LatLng(52.475755075,13.122154914))
                .add(new LatLng(52.475600676,13.121774023))
                .add(new LatLng(52.475491772,13.121525072))
                .add(new LatLng(52.475437038,13.121334371))
                .add(new LatLng(52.475344533,13.120717296))

        );


        /**
         * Zurück Button um wieder zur Routenübersicht zu gelangen!
         */
        Button routenoverview = (Button) findViewById(R.id.routenoverview);
        routenoverview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GatowActivity.this, MainActivity.class));

            }
        });

        /**
         * Routendetails ist ein ToggleButton, der beim Aktivieren ein Fragment added, in dem die
         * Details der Route angezeigt werden (KM, dauert etc..)
         */
        ToggleButton routendetails = (ToggleButton) findViewById(R.id.routendetails);
        routendetails.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RoutenDetailsFragmentGatow routenDetailsFragment = new RoutenDetailsFragmentGatow();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                if(isChecked){
                    transaction.replace(R.id.activity_maplayout, routenDetailsFragment);
                    transaction.show(routenDetailsFragment);
                    transaction.commit();

                }else{
                    /**
                     * Leider löst das replacen vom detailsFragment einen absturz aus , deswegen wurde
                     * es erstmal mit einem Intent gelöst.
                     */
                    startActivity(new Intent(getIntent()));
                }
            }
        });

    }
    public void onClickStart(View v){
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(START_LOCATION,15);
        map.animateCamera(update);

    }
}
