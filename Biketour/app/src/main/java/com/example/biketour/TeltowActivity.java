package com.example.biketour;

import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class TeltowActivity extends FragmentActivity {

    public GoogleMap map;
    public final LatLng START_LOCATION = new LatLng(52.517582451,13.39943695);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teltow);

        map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.addMarker(new MarkerOptions().position(START_LOCATION));
        map.setMyLocationEnabled(true);

        map.addPolyline(new PolylineOptions().geodesic(true)
                .add(new LatLng(52.517582451,13.39943695))
                .add(new LatLng(52.517537519,13.399437182))
                .add(new LatLng(52.517241557,13.399748063))
                .add(new LatLng(52.516416376,13.400577243))
                .add(new LatLng(52.516111425,13.400888155))
                .add(new LatLng(52.516011758,13.400461473))
                .add(new LatLng(52.515866959,13.39993191))
                .add(new LatLng(52.515794446,13.399608208))
                .add(new LatLng(52.515794417,13.399593478))
                .add(new LatLng(52.51577636,13.399549379))
                .add(new LatLng(52.515758217,13.399461088))
                .add(new LatLng(52.515576844,13.398607647))
                .add(new LatLng(52.515513228,13.398239711))
                .add(new LatLng(52.515440768,13.397945475))
                .add(new LatLng(52.515331733,13.397327358))
                .add(new LatLng(52.515276785,13.396797345))
                .add(new LatLng(52.514396335,13.39691978))
                .add(new LatLng(52.514099952,13.397009706))
                .add(new LatLng(52.513839457,13.397069984))
                .add(new LatLng(52.513596907,13.397115438))
                .add(new LatLng(52.513417262,13.397160563))
                .add(new LatLng(52.512817387,13.398312604))
                .add(new LatLng(52.512656282,13.398652221))
                .add(new LatLng(52.512656254,13.398637491))
                .add(new LatLng(52.51238782,13.399242792))
                .add(new LatLng(52.512092874,13.400083893))
                .add(new LatLng(52.51201239,13.400290519))
                .add(new LatLng(52.511600725,13.401191121))
                .add(new LatLng(52.511547086,13.401338688))
                .add(new LatLng(52.511484321,13.401412655))
                .add(new LatLng(52.511484321,13.401412655))
                .add(new LatLng(52.511484321,13.401412655))
                .add(new LatLng(52.511314165,13.401722838))
                .add(new LatLng(52.510990202,13.401488826))
                .add(new LatLng(52.510298295,13.401521819))
                .add(new LatLng(52.510333569,13.401168146))
                .add(new LatLng(52.510108316,13.400859993))
                .add(new LatLng(52.509922446,13.397664811))
                .add(new LatLng(52.509690546,13.393954393))
                .add(new LatLng(52.50806439,13.394169173))
                .add(new LatLng(52.507884775,13.394229033))
                .add(new LatLng(52.506986412,13.394381047))
                .add(new LatLng(52.504596803,13.394805995))
                .add(new LatLng(52.504193806,13.390964413))
                .add(new LatLng(52.503250513,13.391116722))
                .add(new LatLng(52.502154508,13.391299292))
                .add(new LatLng(52.501875955,13.391315505))
                .add(new LatLng(52.501829291,13.390446918))
                .add(new LatLng(52.501801067,13.38981385))
                .add(new LatLng(52.501745849,13.389166202))
                .add(new LatLng(52.501645696,13.388518799))
                .add(new LatLng(52.501627545,13.388430541))
                .add(new LatLng(52.501382174,13.387077086))
                .add(new LatLng(52.501300192,13.386532674))
                .add(new LatLng(52.501227582,13.38617965))
                .add(new LatLng(52.500391086,13.385816074))
                .add(new LatLng(52.498979211,13.385337858))
                .add(new LatLng(52.498457632,13.385164015))
                .add(new LatLng(52.498340686,13.385105755))
                .add(new LatLng(52.495283036,13.384032941))
                .add(new LatLng(52.493934119,13.383583937))
                .add(new LatLng(52.493574445,13.383482857))
                .add(new LatLng(52.492270458,13.38303364))
                .add(new LatLng(52.490498774,13.382395647))
                .add(new LatLng(52.48986926,13.382178303))
                .add(new LatLng(52.48961118,13.3790734))
                .add(new LatLng(52.489408463,13.376689591))
                .add(new LatLng(52.48928902,13.375453632))
                .add(new LatLng(52.4892248,13.374835681))
                .add(new LatLng(52.488621699,13.374368032))
                .add(new LatLng(52.487874276,13.37365096))
                .add(new LatLng(52.487873615,13.373341815))
                .add(new LatLng(52.488020908,13.37079416))
                .add(new LatLng(52.488117495,13.369748376))
                .add(new LatLng(52.488232432,13.368879137))
                .add(new LatLng(52.488276241,13.368363628))
                .add(new LatLng(52.488269096,13.365110213))
                .add(new LatLng(52.488266782,13.364064999))
                .add(new LatLng(52.48857997,13.363459547))
                .add(new LatLng(52.488955962,13.362809546))
                .add(new LatLng(52.489117227,13.362587754))
                .add(new LatLng(52.489260453,13.362336625))
                .add(new LatLng(52.489385837,13.362144489))
                .add(new LatLng(52.489295445,13.361909481))
                .add(new LatLng(52.488933443,13.36077808))
                .add(new LatLng(52.488680131,13.360028802))
                .add(new LatLng(52.488580582,13.35972025))
                .add(new LatLng(52.488598422,13.359661255))
                .add(new LatLng(52.488714649,13.359395561))
                .add(new LatLng(52.488821922,13.359144642))
                .add(new LatLng(52.488813002,13.35917414))
                .add(new LatLng(52.488920075,13.35883489))
                .add(new LatLng(52.48860488,13.358542372))
                .add(new LatLng(52.488720536,13.35802641))
                .add(new LatLng(52.488710814,13.357702592))
                .add(new LatLng(52.488611561,13.357526538))
                .add(new LatLng(52.487526682,13.35469195))
                .add(new LatLng(52.487174209,13.353825575))
                .add(new LatLng(52.486188368,13.351108318))
                .add(new LatLng(52.48532741,13.34802238))
                .add(new LatLng(52.484719814,13.345700403))
                .add(new LatLng(52.484665476,13.345524105))
                .add(new LatLng(52.484376997,13.345143217))
                .add(new LatLng(52.484205657,13.344894068))
                .add(new LatLng(52.484212679,13.344069692))
                .add(new LatLng(52.484175114,13.343392805))
                .add(new LatLng(52.484056878,13.342804759))
                .add(new LatLng(52.483966024,13.342393181))
                .add(new LatLng(52.483793651,13.341717169))
                .add(new LatLng(52.483308839,13.341911659))
                .add(new LatLng(52.482913751,13.342046687))
                .add(new LatLng(52.482410895,13.342211847))
                .add(new LatLng(52.48224889,13.342109855))
                .add(new LatLng(52.482159131,13.342154593))
                .add(new LatLng(52.481683269,13.342334294))
                .add(new LatLng(52.480902077,13.342604272))
                .add(new LatLng(52.480632693,13.342694321))
                .add(new LatLng(52.480444294,13.342828004))
                .add(new LatLng(52.48016617,13.343021138))
                .add(new LatLng(52.479079963,13.343513831))
                .add(new LatLng(52.478972265,13.343573396))
                .add(new LatLng(52.478855687,13.343677173))
                .add(new LatLng(52.478658089,13.343722595))
                .add(new LatLng(52.478604767,13.343973148))
                .add(new LatLng(52.478334608,13.343739388))
                .add(new LatLng(52.477911079,13.343256407))
                .add(new LatLng(52.477739841,13.343051454))
                .add(new LatLng(52.477514578,13.342802697))
                .add(new LatLng(52.477236384,13.342966384))
                .add(new LatLng(52.476957801,13.342968175))
                .add(new LatLng(52.476400812,13.343045344))
                .add(new LatLng(52.475888756,13.343122222))
                .add(new LatLng(52.475870853,13.343151772))
                .add(new LatLng(52.474765826,13.343291325))
                .add(new LatLng(52.473328473,13.343506587))
                .add(new LatLng(52.47287023,13.343538961))
                .add(new LatLng(52.472645425,13.343481538))
                .add(new LatLng(52.472474892,13.34357093))
                .add(new LatLng(52.472124663,13.343676189))
                .add(new LatLng(52.471334094,13.34378427))
                .add(new LatLng(52.471154257,13.343741275))
                .add(new LatLng(52.471153906,13.343594119))
                .add(new LatLng(52.47113558,13.343447079))
                .add(new LatLng(52.471081238,13.343270838))
                .add(new LatLng(52.470873525,13.342845415))
                .add(new LatLng(52.470585143,13.34250881))
                .add(new LatLng(52.469629701,13.341323021))
                .add(new LatLng(52.469458389,13.341088684))
                .add(new LatLng(52.468755447,13.340269191))
                .add(new LatLng(52.468566302,13.340093838))
                .add(new LatLng(52.468359362,13.339992178))
                .add(new LatLng(52.468125498,13.339905408))
                .add(new LatLng(52.46809811,13.339729008))
                .add(new LatLng(52.46805282,13.339582154))
                .add(new LatLng(52.467935531,13.339391625))
                .add(new LatLng(52.467737255,13.339157481))
                .add(new LatLng(52.46745785,13.338820865))
                .add(new LatLng(52.467124273,13.338381606))
                .add(new LatLng(52.466907878,13.338088731))
                .add(new LatLng(52.466691591,13.337840001))
                .add(new LatLng(52.466538352,13.337649718))
                .add(new LatLng(52.465276524,13.336142451))
                .add(new LatLng(52.464375155,13.335044865))
                .add(new LatLng(52.464204447,13.335060707))
                .add(new LatLng(52.462592646,13.33742543))
                .add(new LatLng(52.462323588,13.337647886))
                .add(new LatLng(52.46213505,13.337722685))
                .add(new LatLng(52.461910458,13.33775358))
                .add(new LatLng(52.461191677,13.337817134))
                .add(new LatLng(52.461164718,13.337817311))
                .add(new LatLng(52.461056627,13.337715031))
                .add(new LatLng(52.460804752,13.337613692))
                .add(new LatLng(52.460499138,13.337586269))
                .add(new LatLng(52.460301397,13.337572851))
                .add(new LatLng(52.460238455,13.337558551))
                .add(new LatLng(52.460004949,13.337618929))
                .add(new LatLng(52.459375962,13.337652471))
                .add(new LatLng(52.45857623,13.337687129))
                .add(new LatLng(52.455268312,13.337355725))
                .add(new LatLng(52.455007378,13.33722504))
                .add(new LatLng(52.4541346,13.336789456))
                .add(new LatLng(52.451678162,13.335540577))
                .add(new LatLng(52.45147995,13.335335953))
                .add(new LatLng(52.45139889,13.335262942))
                .add(new LatLng(52.450210086,13.334226457))
                .add(new LatLng(52.448597991,13.332825133))
                .add(new LatLng(52.448156809,13.33248978))
                .add(new LatLng(52.447409789,13.332038809))
                .add(new LatLng(52.446851705,13.331674838))
                .add(new LatLng(52.446815722,13.331660371))
                .add(new LatLng(52.446185524,13.331208647))
                .add(new LatLng(52.445744263,13.330843914))
                .add(new LatLng(52.445383954,13.33050806))
                .add(new LatLng(52.445213025,13.330435671))
                .add(new LatLng(52.445149493,13.330186075))
                .add(new LatLng(52.445086107,13.329995307))
                .add(new LatLng(52.444659856,13.328453934))
                .add(new LatLng(52.444515032,13.328043116))
                .add(new LatLng(52.444297831,13.327441602))
                .add(new LatLng(52.444026632,13.326811047))
                .add(new LatLng(52.443809835,13.326371316))
                .add(new LatLng(52.443602023,13.32593153))
                .add(new LatLng(52.44347565,13.32571179))
                .add(new LatLng(52.44341237,13.325565156))
                .add(new LatLng(52.443213842,13.325242965))
                .add(new LatLng(52.442943082,13.324788913))
                .add(new LatLng(52.442555307,13.324262135))
                .add(new LatLng(52.442320904,13.323969615))
                .add(new LatLng(52.442131584,13.323735614))
                .add(new LatLng(52.441879282,13.323472634))
                .add(new LatLng(52.441563922,13.323151267))
                .add(new LatLng(52.441176631,13.322815693))
                .add(new LatLng(52.440933389,13.322582073))
                .add(new LatLng(52.440843221,13.322465048))
                .add(new LatLng(52.440753052,13.322348023))
                .add(new LatLng(52.44023062,13.321881044))
                .add(new LatLng(52.439546085,13.321282849))
                .add(new LatLng(52.439248844,13.32102021))
                .add(new LatLng(52.439059594,13.320815646))
                .add(new LatLng(52.43902361,13.32080119))
                .add(new LatLng(52.438717382,13.32053862))
                .add(new LatLng(52.438411115,13.320261348))
                .add(new LatLng(52.438312034,13.320173805))
                .add(new LatLng(52.43791571,13.31982364))
                .add(new LatLng(52.437663513,13.319604821))
                .add(new LatLng(52.437411316,13.319386005))
                .add(new LatLng(52.437330246,13.319313046))
                .add(new LatLng(52.437213077,13.31918152))
                .add(new LatLng(52.436978929,13.318991991))
                .add(new LatLng(52.436789713,13.318802152))
                .add(new LatLng(52.436510287,13.318480604))
                .add(new LatLng(52.436266883,13.318188217))
                .add(new LatLng(52.436068603,13.317969039))
                .add(new LatLng(52.435599572,13.317310636))
                .add(new LatLng(52.435166716,13.316740215))
                .add(new LatLng(52.434914089,13.316359689))
                .add(new LatLng(52.434823876,13.316227989))
                .add(new LatLng(52.434769648,13.31611074))
                .add(new LatLng(52.434715458,13.316008195))
                .add(new LatLng(52.434643295,13.315905776))
                .add(new LatLng(52.434147332,13.315262309))
                .add(new LatLng(52.433443859,13.314311553))
                .add(new LatLng(52.432920772,13.3136095))
                .add(new LatLng(52.432460703,13.312951126))
                .add(new LatLng(52.431946556,13.312234339))
                .add(new LatLng(52.431585763,13.311737015))
                .add(new LatLng(52.431252045,13.311283614))
                .add(new LatLng(52.431081143,13.311226023))
                .add(new LatLng(52.430982134,13.31116792))
                .add(new LatLng(52.430891954,13.311050944))
                .add(new LatLng(52.430630244,13.310641148))
                .add(new LatLng(52.430585115,13.310567959))
                .add(new LatLng(52.43055788,13.310465237))
                .add(new LatLng(52.430539357,13.310259537))
                .add(new LatLng(52.43041319,13.31012812))
                .add(new LatLng(52.430277998,13.309982065))
                .add(new LatLng(52.430025862,13.309792744))
                .add(new LatLng(52.429692728,13.3095599))
                .add(new LatLng(52.428639128,13.308758866))
                .add(new LatLng(52.426910339,13.307521705))
                .add(new LatLng(52.425964802,13.306808192))
                .add(new LatLng(52.425721727,13.306648243))
                .add(new LatLng(52.423821651,13.305221389))
                .add(new LatLng(52.422939166,13.304566315))
                .add(new LatLng(52.421678478,13.303634739))
                .add(new LatLng(52.421363265,13.303387155))
                .add(new LatLng(52.420796024,13.302994427))
                .add(new LatLng(52.420363743,13.302659516))
                .add(new LatLng(52.420129648,13.302499544))
                .add(new LatLng(52.4199585,13.302353811))
                .add(new LatLng(52.419422202,13.303416046))
                .add(new LatLng(52.419250532,13.303079234))
                .add(new LatLng(52.419196251,13.302947344))
                .add(new LatLng(52.419133103,13.302859615))
                .add(new LatLng(52.418970821,13.302669724))
                .add(new LatLng(52.418592135,13.30221685))
                .add(new LatLng(52.418393785,13.301983132))
                .add(new LatLng(52.418069016,13.301529873))
                .add(new LatLng(52.417807354,13.301149647))
                .add(new LatLng(52.41763584,13.300871647))
                .add(new LatLng(52.417355837,13.300359282))
                .add(new LatLng(52.41714805,13.299963972))
                .add(new LatLng(52.41704871,13.299788332))
                .add(new LatLng(52.417003371,13.299641689))
                .add(new LatLng(52.416958072,13.299509744))
                .add(new LatLng(52.416912854,13.299407194))
                .add(new LatLng(52.416867596,13.299289946))
                .add(new LatLng(52.416777201,13.299099545))
                .add(new LatLng(52.416460429,13.29829352))
                .add(new LatLng(52.416334004,13.298073995))
                .add(new LatLng(52.41596265,13.297033233))
                .add(new LatLng(52.415708811,13.296241465))
                .add(new LatLng(52.415537861,13.296169256))
                .add(new LatLng(52.415169328,13.296142616))
                .add(new LatLng(52.414872442,13.296027258))
                .add(new LatLng(52.414557542,13.295897339))
                .add(new LatLng(52.414538664,13.295574146))
                .add(new LatLng(52.414357779,13.295163986))
                .add(new LatLng(52.414285474,13.295017558))
                .add(new LatLng(52.413805593,13.293742538))
                .add(new LatLng(52.413452049,13.292657647))
                .add(new LatLng(52.412909236,13.291383153))
                .add(new LatLng(52.412683321,13.290943974))
                .add(new LatLng(52.41196243,13.290258726))
                .add(new LatLng(52.410977695,13.288443933))
                .add(new LatLng(52.40881001,13.284669168))
                .add(new LatLng(52.408474867,13.283760668))
                .add(new LatLng(52.40768012,13.282414913))
                .add(new LatLng(52.406876285,13.281039888))
                .add(new LatLng(52.404590712,13.276987583))
                .add(new LatLng(52.404326619,13.275814179))
                .add(new LatLng(52.404012394,13.275919526))
                .add(new LatLng(52.403770017,13.276009609))
                .add(new LatLng(52.403437034,13.275850622))
                .add(new LatLng(52.403149071,13.275720666))
                .add(new LatLng(52.402476185,13.273081258))
                .add(new LatLng(52.402113025,13.271849957))
                .add(new LatLng(52.400872875,13.268877325))

        );


        /**
         * Zurück Button um wieder zur Routenübersicht zu gelangen!
         */
        Button routenoverview = (Button) findViewById(R.id.routenoverview);
        routenoverview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TeltowActivity.this, MainActivity.class));

            }
        });

        /**
         * Routendetails ist ein ToggleButton, der beim Aktivieren ein Fragment added, in dem die
         * Details der Route angezeigt werden (KM, dauert etc..)
         */
        ToggleButton routendetails = (ToggleButton) findViewById(R.id.routendetails);
        routendetails.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RoutenDetailsFragmentTeltow routenDetailsFragment = new RoutenDetailsFragmentTeltow();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                if(isChecked){
                    transaction.replace(R.id.activity_maplayout, routenDetailsFragment);
                    transaction.show(routenDetailsFragment);
                    transaction.commit();

                }else{
                    /**
                     * Leider löst das replacen vom detailsFragment einen absturz aus , deswegen wurde
                     * es erstmal mit einem Intent gelöst.
                     */
                    startActivity(new Intent(getIntent()));
                }
            }
        });

    }
    public void onClickStart(View v){
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(START_LOCATION,15);
        map.animateCamera(update);

    }
}
