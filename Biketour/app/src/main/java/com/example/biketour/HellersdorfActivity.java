package com.example.biketour;

import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class HellersdorfActivity extends FragmentActivity {

    public GoogleMap map;
    public final LatLng START_LOCATION = new LatLng(52.516522103,13.649528739);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hellersdorf);

        map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
        map.addMarker(new MarkerOptions().position(START_LOCATION));
        map.setMyLocationEnabled(true);

        map.addPolyline(new PolylineOptions().geodesic(true)
                .add(new LatLng(52.516522103,13.649528739))
                .add(new LatLng(52.518966724,13.648086308))
                .add(new LatLng(52.519236349,13.647939122))
                .add(new LatLng(52.519703714,13.647585782))
                .add(new LatLng(52.520701312,13.64708536))
                .add(new LatLng(52.521411315,13.646717378))
                .add(new LatLng(52.520755527,13.645302779))
                .add(new LatLng(52.522103891,13.642739856))
                .add(new LatLng(52.522778053,13.641458336))
                .add(new LatLng(52.523353322,13.640397743))
                .add(new LatLng(52.520954275,13.6367581))
                .add(new LatLng(52.521664312,13.635550192))
                .add(new LatLng(52.522329405,13.634327504))
                .add(new LatLng(52.522922582,13.633296298))
                .add(new LatLng(52.52445941,13.63057083))
                .add(new LatLng(52.524549282,13.630364568))
                .add(new LatLng(52.524585233,13.630217236))
                .add(new LatLng(52.52459423,13.629878365))
                .add(new LatLng(52.524540321,13.629480555))
                .add(new LatLng(52.524333654,13.628198728))
                .add(new LatLng(52.52428873,13.627299985))
                .add(new LatLng(52.524270759,13.626415977))
                .add(new LatLng(52.524252781,13.625576171))
                .add(new LatLng(52.524297709,13.625119432))
                .add(new LatLng(52.524369588,13.624250154))
                .add(new LatLng(52.52447741,13.623528205))
                .add(new LatLng(52.524809855,13.621774879))
                .add(new LatLng(52.524863764,13.621509667))
                .add(new LatLng(52.525070408,13.620507749))
                .add(new LatLng(52.525142287,13.620257266))
                .add(new LatLng(52.52519619,13.619947849))
                .add(new LatLng(52.525330949,13.619284807))
                .add(new LatLng(52.525645383,13.617870298))
                .add(new LatLng(52.525663354,13.617826093))
                .add(new LatLng(52.526004704,13.616116876))
                .add(new LatLng(52.526238207,13.614555004))
                .add(new LatLng(52.526453725,13.613081527))
                .add(new LatLng(52.526866815,13.610723907))
                .add(new LatLng(52.527037388,13.609486157))
                .add(new LatLng(52.527513186,13.606376983))
                .add(new LatLng(52.527584979,13.605802302))
                .add(new LatLng(52.527916996,13.603194107))
                .add(new LatLng(52.528096426,13.601720538))
                .add(new LatLng(52.527997379,13.600807041))
                .add(new LatLng(52.52798838,13.600748108))
                .add(new LatLng(52.528114065,13.600173379))
                .add(new LatLng(52.528203861,13.599863895))
                .add(new LatLng(52.528697134,13.595796751))
                .add(new LatLng(52.528786794,13.595015735))
                .add(new LatLng(52.52918129,13.591758994))
                .add(new LatLng(52.529441525,13.590506297))
                .add(new LatLng(52.529549251,13.590137828))
                .add(new LatLng(52.529576147,13.589931512))
                .add(new LatLng(52.529549151,13.589813653))
                .add(new LatLng(52.529459266,13.589754787))
                .add(new LatLng(52.529378374,13.58971065))
                .add(new LatLng(52.529333423,13.589651746))
                .add(new LatLng(52.52932439,13.589504402))
                .add(new LatLng(52.529539711,13.588384344))
                .add(new LatLng(52.52959354,13.588104328))
                .add(new LatLng(52.529611426,13.587839078))
                .add(new LatLng(52.529602358,13.587588587))
                .add(new LatLng(52.52941336,13.586748848))
                .add(new LatLng(52.528990497,13.585305197))
                .add(new LatLng(52.528936536,13.585187367))
                .add(new LatLng(52.528801645,13.584922264))
                .add(new LatLng(52.528828427,13.58442125))
                .add(new LatLng(52.528693481,13.5840088))
                .add(new LatLng(52.528351789,13.583449207))
                .add(new LatLng(52.527803326,13.582683543))
                .add(new LatLng(52.52728183,13.58194734))
                .add(new LatLng(52.527030117,13.581711845))
                .add(new LatLng(52.526355723,13.580666407))
                .add(new LatLng(52.526067975,13.580209951))
                .add(new LatLng(52.525690377,13.579797798))
                .add(new LatLng(52.52552856,13.579650631))
                .add(new LatLng(52.525528525,13.579562228))
                .add(new LatLng(52.525752853,13.578722153))
                .add(new LatLng(52.525869475,13.57822107))
                .add(new LatLng(52.525941258,13.577955779))
                .add(new LatLng(52.526147669,13.577277782))
                .add(new LatLng(52.526255354,13.576909309))
                .add(new LatLng(52.526345077,13.576570322))
                .add(new LatLng(52.526506572,13.575951301))
                .add(new LatLng(52.526650142,13.575450172))
                .add(new LatLng(52.526775807,13.575111137))
                .add(new LatLng(52.526991252,13.574580447))
                .add(new LatLng(52.52719769,13.57400556))
                .add(new LatLng(52.527251537,13.573843417))
                .add(new LatLng(52.527071727,13.573666819))
                .add(new LatLng(52.526999815,13.573622703))
                .add(new LatLng(52.526990715,13.573372229))
                .add(new LatLng(52.527035328,13.572664925))
                .add(new LatLng(52.525750108,13.572357086))
                .add(new LatLng(52.525839476,13.571281394))
                .add(new LatLng(52.525183388,13.571134881))
                .add(new LatLng(52.524410473,13.570988519))
                .add(new LatLng(52.524562679,13.569794908))
                .add(new LatLng(52.524759682,13.568350758))
                .add(new LatLng(52.524956696,13.56696553))
                .add(new LatLng(52.525064102,13.566110829))
                .add(new LatLng(52.525144587,13.565344563))
                .add(new LatLng(52.525540034,13.565417682))
                .add(new LatLng(52.525548814,13.565019855))
                .add(new LatLng(52.525566642,13.564739886))
                .add(new LatLng(52.525539419,13.564238972))
                .add(new LatLng(52.525521306,13.563973788))
                .add(new LatLng(52.525503114,13.563561265))
                .add(new LatLng(52.525457929,13.563089846))
                .add(new LatLng(52.525376788,13.562603746))
                .add(new LatLng(52.525322613,13.562132343))
                .add(new LatLng(52.52518724,13.561086443))
                .add(new LatLng(52.525150785,13.560173004))
                .add(new LatLng(52.525114448,13.559480573))
                .add(new LatLng(52.525158961,13.558743817))
                .add(new LatLng(52.525283781,13.557034503))
                .add(new LatLng(52.525238692,13.556769366))
                .add(new LatLng(52.525354675,13.555354734))
                .add(new LatLng(52.525720428,13.551007627))
                .add(new LatLng(52.525827585,13.549946596))
                .add(new LatLng(52.525952676,13.548841326))
                .add(new LatLng(52.526042338,13.548531752))
                .add(new LatLng(52.526114114,13.548354816))
                .add(new LatLng(52.526203755,13.548015772))
                .add(new LatLng(52.526203726,13.547971569))
                .add(new LatLng(52.526113743,13.547794921))
                .add(new LatLng(52.526041693,13.547559305))
                .add(new LatLng(52.52597855,13.547205802))
                .add(new LatLng(52.52593337,13.546837532))
                .add(new LatLng(52.52588706,13.544819057))
                .add(new LatLng(52.525849598,13.542653229))
                .add(new LatLng(52.525831047,13.541842895))
                .add(new LatLng(52.525821072,13.540472653))
                .add(new LatLng(52.52591069,13.540133595))
                .add(new LatLng(52.525784577,13.539721291))
                .add(new LatLng(52.525793466,13.539588668))
                .add(new LatLng(52.525738564,13.53826272))
                .add(new LatLng(52.525674292,13.536450574))
                .add(new LatLng(52.525674281,13.53643584))
                .add(new LatLng(52.525591767,13.534314325))
                .add(new LatLng(52.525564312,13.533680825))
                .add(new LatLng(52.525527305,13.532340119))
                .add(new LatLng(52.525499354,13.531102532))
                .add(new LatLng(52.525481059,13.530704757))
                .add(new LatLng(52.525471413,13.529894415))
                .add(new LatLng(52.525470761,13.529098788))
                .add(new LatLng(52.525479674,13.529010364))
                .add(new LatLng(52.525496793,13.527978955))
                .add(new LatLng(52.525514175,13.527271689))
                .add(new LatLng(52.52553154,13.526549689))
                .add(new LatLng(52.525566849,13.525798179))
                .add(new LatLng(52.525602089,13.524972999))
                .add(new LatLng(52.525673066,13.523911992))
                .add(new LatLng(52.525744098,13.52292465))
                .add(new LatLng(52.525806266,13.522084666))
                .add(new LatLng(52.525815005,13.5218047))
                .add(new LatLng(52.525814953,13.521745764))
                .add(new LatLng(52.525823376,13.521112182))
                .add(new LatLng(52.525894053,13.519756483))
                .add(new LatLng(52.525911707,13.519402824))
                .add(new LatLng(52.525938493,13.519211216))
                .add(new LatLng(52.525992265,13.51904901))
                .add(new LatLng(52.526036647,13.518444805))
                .add(new LatLng(52.526054256,13.518046942))
                .add(new LatLng(52.526062999,13.517781707))
                .add(new LatLng(52.526080756,13.517545918))
                .add(new LatLng(52.526080484,13.517251237))
                .add(new LatLng(52.526062211,13.516927132))
                .add(new LatLng(52.526061938,13.516632451))
                .add(new LatLng(52.5260348,13.516440976))
                .add(new LatLng(52.526016662,13.516264213))
                .add(new LatLng(52.52598951,13.516058004))
                .add(new LatLng(52.525962344,13.515837062))
                .add(new LatLng(52.525917135,13.515542495))
                .add(new LatLng(52.525763698,13.514835651))
                .add(new LatLng(52.525682289,13.514275967))
                .add(new LatLng(52.525636739,13.51362779))
                .add(new LatLng(52.525636048,13.512905829))
                .add(new LatLng(52.525537139,13.512847149))
                .add(new LatLng(52.525492122,13.512758862))
                .add(new LatLng(52.525483008,13.512626281))
                .add(new LatLng(52.525464051,13.511609692))
                .add(new LatLng(52.525425558,13.509016632))
                .add(new LatLng(52.525416161,13.50860411))
                .add(new LatLng(52.525388627,13.508029563))
                .add(new LatLng(52.525406186,13.507616968))
                .add(new LatLng(52.525414742,13.507189663))
                .add(new LatLng(52.525450346,13.506850687))
                .add(new LatLng(52.525521925,13.50654108))
                .add(new LatLng(52.525584592,13.506305167))
                .add(new LatLng(52.525692161,13.506039662))
                .add(new LatLng(52.525548015,13.505686443))
                .add(new LatLng(52.52552098,13.505612847))
                .add(new LatLng(52.525349768,13.505156569))
                .add(new LatLng(52.525214652,13.504847531))
                .add(new LatLng(52.525016978,13.504877545))
                .add(new LatLng(52.524423911,13.504923385))
                .add(new LatLng(52.523956701,13.505013076))
                .add(new LatLng(52.523417493,13.504999831))
                .add(new LatLng(52.523417493,13.504999831))
                .add(new LatLng(52.52321065,13.504853071))
                .add(new LatLng(52.523003823,13.504721046))
                .add(new LatLng(52.52279689,13.50448589))
                .add(new LatLng(52.52279689,13.50448589))
                .add(new LatLng(52.522365093,13.504059835))
                .add(new LatLng(52.521933066,13.503412798))
                .add(new LatLng(52.521716805,13.502853564))
                .add(new LatLng(52.521626414,13.502352909))
                .add(new LatLng(52.521616745,13.501704702))
                .add(new LatLng(52.521633986,13.501012221))
                .add(new LatLng(52.521633218,13.500290326))
                .add(new LatLng(52.521677315,13.499509371))
                .add(new LatLng(52.520338223,13.499424842))
                .add(new LatLng(52.520399825,13.498216624))
                .add(new LatLng(52.520417109,13.49758309))
                .add(new LatLng(52.520452267,13.496861109))
                .add(new LatLng(52.520487062,13.495815019))
                .add(new LatLng(52.520522044,13.494945713))
                .add(new LatLng(52.520584359,13.494415166))
                .add(new LatLng(52.520682915,13.494149689))
                .add(new LatLng(52.520699995,13.493354094))
                .add(new LatLng(52.52069041,13.492823761))
                .add(new LatLng(52.520689793,13.492278668))
                .add(new LatLng(52.520706271,13.490967444))
                .add(new LatLng(52.52072282,13.48972988))
                .add(new LatLng(52.520730661,13.488742791))
                .add(new LatLng(52.520746979,13.487328438))
                .add(new LatLng(52.520800151,13.48669478))
                .add(new LatLng(52.520844526,13.486223204))
                .add(new LatLng(52.520844193,13.485943291))
                .add(new LatLng(52.520842309,13.484366936))
                .add(new LatLng(52.520850924,13.484057529))
                .add(new LatLng(52.520794521,13.482009918))
                .add(new LatLng(52.520801499,13.480374607))
                .add(new LatLng(52.520630171,13.479903743))
                .add(new LatLng(52.520449488,13.47913827))
                .add(new LatLng(52.52030384,13.477650809))
                .add(new LatLng(52.520303488,13.477370899))
                .add(new LatLng(52.520648939,13.47343621))
                .add(new LatLng(52.520683585,13.472434293))
                .add(new LatLng(52.520638133,13.472036681))
                .add(new LatLng(52.520510447,13.470608102))
                .add(new LatLng(52.520428224,13.469591871))
                .add(new LatLng(52.520318205,13.467956992))
                .add(new LatLng(52.520162852,13.466042375))
                .add(new LatLng(52.519943223,13.463155694))
                .add(new LatLng(52.519888346,13.462463492))
                .add(new LatLng(52.519339839,13.462229827))
                .add(new LatLng(52.517981948,13.461571981))
                .add(new LatLng(52.517217495,13.461147642))
                .add(new LatLng(52.515490964,13.460358676))
                .add(new LatLng(52.515329102,13.460285634))
                .add(new LatLng(52.515399978,13.459563571))
                .add(new LatLng(52.515860383,13.454744891))
                .add(new LatLng(52.515877864,13.454406016))
                .add(new LatLng(52.515957927,13.453845936))
                .add(new LatLng(52.516046478,13.452947013))
                .add(new LatLng(52.516267901,13.450751247))
                .add(new LatLng(52.516639315,13.446742945))
                .add(new LatLng(52.516993557,13.443338621))
                .add(new LatLng(52.517223326,13.440848101))
                .add(new LatLng(52.517621317,13.43681005))
                .add(new LatLng(52.517975153,13.433331914))
                .add(new LatLng(52.518001631,13.43303717))
                .add(new LatLng(52.5180728,13.432594914))
                .add(new LatLng(52.518347325,13.430133539))
                .add(new LatLng(52.518516648,13.429278351))
                .add(new LatLng(52.518686974,13.42902715))
                .add(new LatLng(52.518767656,13.428908935))
                .add(new LatLng(52.518857053,13.428628631))
                .add(new LatLng(52.518928304,13.428245286))
                .add(new LatLng(52.518882803,13.427906662))
                .add(new LatLng(52.518828587,13.427730126))
                .add(new LatLng(52.518855225,13.427538493))
                .add(new LatLng(52.519132168,13.426564943))
                .add(new LatLng(52.519310852,13.425945394))
                .add(new LatLng(52.519498493,13.425311066))
                .add(new LatLng(52.519686232,13.424735661))
                .add(new LatLng(52.519882828,13.42408655))
                .add(new LatLng(52.520097267,13.42336369))
                .add(new LatLng(52.520133036,13.4232604))
                .add(new LatLng(52.520365418,13.422522716))
                .add(new LatLng(52.520553067,13.421903091))
                .add(new LatLng(52.520919445,13.420708064))
                .add(new LatLng(52.521929213,13.417447409))
                .add(new LatLng(52.522215138,13.416517882))
                .add(new LatLng(52.52205304,13.416327129))
                .add(new LatLng(52.521638848,13.415872395))
                .add(new LatLng(52.520855602,13.415080594))
                .add(new LatLng(52.520666464,13.414845785))
                .add(new LatLng(52.520531297,13.414640183))
                .add(new LatLng(52.520224778,13.414096568))
                .add(new LatLng(52.520080385,13.413758426))
                .add(new LatLng(52.520439183,13.413388386))
                .add(new LatLng(52.520627289,13.413048632))
                .add(new LatLng(52.520959045,13.412634516))
                .add(new LatLng(52.520859847,13.412443477))
                .add(new LatLng(52.520580305,13.411914474))
                .add(new LatLng(52.519109661,13.408724841))
                .add(new LatLng(52.518676651,13.407828352))
                .add(new LatLng(52.51842402,13.407284539))
                .add(new LatLng(52.518934829,13.406515945))
                .add(new LatLng(52.51943673,13.405791574))
                .add(new LatLng(52.519732569,13.405407058))
                .add(new LatLng(52.520055202,13.40493401))
                .add(new LatLng(52.519414863,13.403714485))
                .add(new LatLng(52.519027026,13.402965126))
                .add(new LatLng(52.518927757,13.402744654))
                .add(new LatLng(52.518114799,13.400553797))
                .add(new LatLng(52.518015441,13.40028914))
                .add(new LatLng(52.517834639,13.399730277))
                .add(new LatLng(52.517716938,13.399274212))

        );


        /**
         * Zurück Button um wieder zur Routenübersicht zu gelangen!
         */
        Button routenoverview = (Button) findViewById(R.id.routenoverview);
        routenoverview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HellersdorfActivity.this, MainActivity.class));

            }
        });

        /**
         * Routendetails ist ein ToggleButton, der beim Aktivieren ein Fragment added, in dem die
         * Details der Route angezeigt werden (KM, dauert etc..)
         */
        ToggleButton routendetails = (ToggleButton) findViewById(R.id.routendetails);
        routendetails.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RoutenDetailsFragmentHellersdorf routenDetailsFragment = new RoutenDetailsFragmentHellersdorf();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                if(isChecked){
                    transaction.replace(R.id.activity_maplayout, routenDetailsFragment);
                    transaction.show(routenDetailsFragment);
                    transaction.commit();

                }else{
                    /**
                     * Leider löst das replacen vom detailsFragment einen absturz aus , deswegen wurde
                     * es erstmal mit einem Intent gelöst.
                     */
                    startActivity(new Intent(getIntent()));
                }
            }
        });

    }
    public void onClickStart(View v){
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(START_LOCATION,15);
        map.animateCamera(update);

    }
}
