package com.example.biketour;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button wanseebutton = (Button) findViewById(R.id.wanseebutton);
        wanseebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MapActivity.class));

            }
        });

        Button gatowbutton = (Button) findViewById(R.id.gatowbutton);
        gatowbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, GatowActivity.class));

            }
        });

        Button hellersdorfbutton = (Button) findViewById(R.id.hellersdorfbutton);
        hellersdorfbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HellersdorfActivity.class));

            }
        });

        Button spandaubutton = (Button) findViewById(R.id.spandaubutton);
        spandaubutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SpandauActivity.class));

            }
        });

        Button teltowbutton = (Button) findViewById(R.id.teltowbutton);
        teltowbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TeltowActivity.class));

            }
        });

        Button useudombutton = (Button) findViewById(R.id.usedombutton);
        useudombutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, UsedomActivity.class));

            }
        });

        Button mauersuedbutton = (Button) findViewById(R.id.mauersuedbutton);
        mauersuedbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SuedActivity.class));

            }
        });

        Button mauerwestbutton = (Button) findViewById(R.id.mauerwestbutton);
        mauerwestbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, WestActivity.class));

            }
        });
    }
}

